# SD42 - Morning Music
Run the bot in the morning-music channel where members can request a number from spotify

## Getting started
Install the required packages by running:
```sh
poetry install
```

Run the bot as follows:
```sh
poetry run python morning_music/main.py
```

Be sure to properly configure your discord server so the bot can access the channels, messages and create threads. Details can be found here: https://www.writebots.com/how-to-make-a-discord-bot/

How to add this application to your spotify developer enviroment: https://developer.spotify.com/dashboard/

In this repository you will find a *.env* file which contains the following environment variables needed for the bot to run:
```sh
#Spotify
SPOTIFY_CLIENT_ID = <your spotify client id>
SPOTIFY_CLIENT_SECRET = <your spotify client secret>
SPOTIFY_PLAYLIST_ID = <your spotify playlist>
#Discord
DISCORD_TOKEN = <your discord dev-token>
DISCORD_GUILD = <guild to listen on for messages>
DISCORD_CHANNEL = <channel to listen on for messages>
DISCORD_ADMIN = <Admin role in your server to be able to use admin commands>
```

Note: You should provide a correct Discord secret so the bot can connect to your server. Never commit your secret to this repository! The default admin role is 'Teacher' (which should be a role on your Discord server). You can change this to your own role  if you want.

## Required intents from [Discord developer portal](https://discord.com/developers/applications/)
- `Server members intent`
- `Message content intent`

## Required server role permissions
- `Send messages`
- `Read chat history`
- `Manage messsages`
- `Manage expressions`

# Contribution
All the suggestions for better code and features are made as a issue in the project.
To contribute, clone the project, make the code beter and request a pull-request.
