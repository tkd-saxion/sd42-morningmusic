import discord
import os
from dotenv import load_dotenv
from discord import app_commands

load_dotenv()

DISCORD_GUILD = os.environ.get("DISCORD_GUILD")

class MorningMusicBot(discord.Client):
    def __init__(self):
        intents = discord.Intents.default()
        intents.members = True
        super().__init__(intents=intents)
        self.tree = app_commands.CommandTree(self)
        self.my_guild = discord.Object(id=DISCORD_GUILD)

    async def setup_hook(self):
        self.tree.copy_global_to(guild=self.my_guild)
        await self.tree.sync(guild=self.my_guild)