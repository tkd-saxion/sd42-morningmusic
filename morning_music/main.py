import os
from dotenv import load_dotenv

from morning_music import bot, music_request

import discord
from discord import app_commands
from discord.ext import tasks

import spotipy
from spotipy.oauth2 import SpotifyClientCredentials, SpotifyOAuth
import json
import random
import datetime

load_dotenv()

DISCORD_TOKEN = os.environ.get("DISCORD_TOKEN")
DISCORD_ADMIN = os.environ.get("DISCORD_ADMIN")
DISCORD_CHANNEL = os.environ.get("DISCORD_CHANNEL")
SPOTIFY_CLIENT_ID = os.environ.get("SPOTIFY_CLIENT_ID")
SPOTIFY_CLIENT_SECRET = os.environ.get("SPOTIFY_CLIENT_SECRET")
SPOTIFY_PLAYLIST_ID = os.environ.get("SPOTIFY_PLAYLIST_ID")

#Votes
UP_VOTE = '👍'
DOWN_VOTE = '👎'

#Request length
MAX_TRACK_LENGTH = 10 #Minutes

#Goodmornings
greetings = [
    "Goodmorning party-people!",
    "Make the choice of your live, ughm this morning.",
    "Let me sleep....",
    "Never gona give you a choice...."
]

#Scheduler
utc = datetime.timezone.utc    
timezone = datetime.datetime.now(utc).astimezone()
times = [
    datetime.time(hour=8, minute=30, tzinfo=timezone.tzinfo),
    datetime.time(hour=9, tzinfo=timezone.tzinfo),
    datetime.time(hour=9, minute=5, tzinfo=timezone.tzinfo) #Failsafe check
]

#List of users with there requests for morning music
collection = music_request.MorningRequests()

#Make connection with spotify
spotify_credentials = SpotifyClientCredentials(client_id=SPOTIFY_CLIENT_ID, client_secret=SPOTIFY_CLIENT_SECRET)
spotify_client = spotipy.client.Spotify(client_credentials_manager=spotify_credentials)

#Setting up Discord
discord_client = bot.MorningMusicBot()

#Functions
#Extract Spotify track from url/id
async def try_extracting_track(search: str):
    '''Extracts a track incase of a valid url or id'''
    #Try finding a track if search is a spotify url link
    if "open.spotify.com/track" in search:
        url_parts = search.split("/")
        for part_index, url_part in enumerate(url_parts):
            #Check if url part is at 'track'
            if not "track" == url_part:
                continue
            
            #Check if url goes past 'track/'
            if part_index > len(url_parts):
                break
            
            #Get Spotify track from id
            try:
                return spotify_client.track("spotify:track:" + url_parts[part_index + 1], market='NL')
            except:
                break
        
        return None
    
    #Try finding a track if search is a spotify id
    try:
        return spotify_client.track("spotify:track:" + search, market='NL')
    except: 
        return None

#Search with autocomplete thru Spotify
async def track_autocomplete(interaction: discord.Interaction, search: str):    
    '''Handles autocompletion when trying to search a track'''
    if is_not_taking_requests(interaction):
        return []
    
    #Check if the submitted search is a track url/id that can be extracted from
    track = await try_extracting_track(search)
    if track:
        return [app_commands.Choice(name=f"{track['name']} - {track['artists'][0]['name']}", value=track['uri'])]
    
    #Perform regular track autocompletion
    spotify_results = None
    if  len(search) <= 2 or len(search) > 80:
        return []

    try:
        spotify_results = spotify_client.search(q=search, limit=10, type="track")
    except spotipy.exceptions.SpotifyException as spotify_expection:
        print(f"Code: {spotify_expection.code}, Message: {spotify_expection.msg}")

    if spotify_results:
        return [
            app_commands.Choice(name=f"{track['name']} - {track['artists'][0]['name']}"[:98], value=track['uri'])
            for track in spotify_results['tracks']['items'] if search.lower() in track['name'].lower()
        ]

    return []

def is_admin(interaction: discord.Interaction) -> bool:
    '''Checks if the user that interacts has the admin role'''
    for role in interaction.user.roles:
        if role.name == DISCORD_ADMIN:
            return True
    return False

#Discord check() function
def is_taking_requests(interaction: discord.Interaction) -> bool:
    return collection.is_taking_requests()

def is_not_taking_requests(interaction: discord.Interaction) -> bool:
    return not collection.is_taking_requests()
        
@tasks.loop(time=times)
async def run_weekly_loop(channel : discord.TextChannel):
    #On a wednesday
    if datetime.datetime.now().weekday() == 2:
        if not collection.is_taking_requests():
            collection.set_taking_requests(True)

            print("Started listening to requests")
            await channel.send(random.choice(greetings))

#When discordbot is coming online
@discord_client.event
async def on_ready():
    '''When the bot's comes online'''
    print(f"Bot is online as {discord_client.user.name}")
    channel = discord_client.get_channel(int(DISCORD_CHANNEL))
    if channel:
        await run_weekly_loop.start(channel)

@discord_client.tree.command(description="Manually starts taking in requests")
@app_commands.check(is_admin)
@app_commands.check(is_not_taking_requests)
async def start(interaction: discord.Interaction):
    '''Teacher role has the ability to let the bot start taking in requests'''
    collection.set_taking_requests(True)

    print("Started listening to requests")
    await interaction.response.send_message(random.choice(greetings))

@start.error
async def start_error(interaction: discord.Interaction, error: app_commands.AppCommandError):
    if isinstance(error, app_commands.errors.CheckFailure):
        if not is_admin(interaction): return await interaction.response.send_message(f"You don't have the permission to execute the command /start, you need the role: {DISCORD_ADMIN}", ephemeral=True)

        if collection.is_taking_requests(): return await interaction.response.send_message("Bot is already listening to requests", ephemeral=True)
    
@discord_client.tree.command(description="Manually clears requests and stops taking incoming requests")
@app_commands.check(is_admin)
async def clear(interaction: discord.Interaction):
    '''Teacher role has the ability to manually clear the requests'''
    collection.clear_requests()
    collection.set_taking_requests(False)

    message = "Closed requests and cleared the list"
    print(message)
    await interaction.response.send_message(message)

@clear.error
async def clear_error(interaction: discord.Interaction, error: app_commands.AppCommandError):
    if isinstance(error, app_commands.errors.CheckFailure):
        await interaction.response.send_message(f"You don't have the permission to execute the command /clear, you need the role: {DISCORD_ADMIN}", ephemeral=True)

@discord_client.tree.command(description="Picks a random request and adds it to the spotify playlist")
@app_commands.check(is_admin)
async def drum_ruffle(interaction: discord.Interaction):
    '''Teacher role has the ability to request the /drum_ruffle to reveal a random requests and added to the spotify playlist'''

    #Kick Timothy
    if interaction.user.id == 805449876216873002:
        dm = interaction.user.create_dm()
        await dm.send("Timothy, stop met spammen!")
        return

    #Return false if user is not in configurated channel
    if interaction.channel_id != int(DISCORD_CHANNEL):
        await interaction.response.send_message("You are in the wrong channel, bye bye!")
        return

    #if user has the admin role
    is_admin = [role for role in interaction.user.roles if role.name == DISCORD_ADMIN]
    if not is_admin:    
        await interaction.response.send_message("You don't have the role to execute this command")
        return

    #Retrieve a random choice of all requests
    if len(collection) == 0:
        await interaction.response.send_message("No requests made for now")
        return

    #Return the correct 
    picked = collection.pick_a_request()
    user : discord.User = picked.user

    print(f"The drum-ruffle has picked the requests of {user.display_name}: {picked.track}")
    
    sp_user = spotipy.client.Spotify(auth_manager=SpotifyOAuth(client_id=SPOTIFY_CLIENT_ID, client_secret=SPOTIFY_CLIENT_SECRET, redirect_uri="http://localhost:8080", scope='playlist-modify-public playlist-modify-private'))
    sp_user.playlist_add_items(SPOTIFY_PLAYLIST_ID, items=[picked.track.id])

    print("Added to the playlist")
    await interaction.response.send_message(f"{user.display_name} has {picked.track} requested. Also submitted to the playlist")

    collection.set_taking_requests(False)

    #Clear up requests
    collection.clear_requests()
    print("Requests cleared")

@drum_ruffle.error
async def drum_ruffle_error(interaction: discord.Interaction, error: app_commands.AppCommandError):
    '''If you don't have the correct roles'''
    if isinstance(error, app_commands.errors.CheckFailure):
        await interaction.response.send_message(f"You don't have the permission to execute the command /drum_ruffle, you need the role: {DISCORD_ADMIN}", ephemeral=True)

def handle_vote(reaction: discord.Reaction, user: discord.User):
    #Ignore if message is not a music request
    request = collection.get_request(reaction.message.id)
    if not request:
        return
    
    #Ignore if user is the requester
    if user.id == request.user.id:
        return
    
    #Check if emoji reaction is a thumbs up or thumbs down
    if not(reaction.emoji == UP_VOTE or reaction.emoji == DOWN_VOTE):
        return
        
    return request
        
@discord_client.event
async def on_reaction_add(reaction: discord.Reaction, user: discord.User):
    request = handle_vote(reaction, user)
    
    if not request: return
    
    if reaction.emoji == UP_VOTE: await collection.change_vote(user, request, 1)
    else: await collection.change_vote(user, request, -1)
           
    #Print message
    message = f'{user.display_name} voted {reaction.emoji} on: {request.track} ({request.get_total_votes()} votes)'
    print(message)
    
@discord_client.event
async def on_reaction_remove(reaction: discord.Reaction, user: discord.User):
    request = handle_vote(reaction, user)
    
    if not request: return
    
    if reaction.emoji == UP_VOTE: await collection.change_vote(user, request)
    else: await collection.change_vote(user, request)
    
    #Print message
    message = f'{user.display_name} removed their {reaction.emoji} on: {request.track} ({request.get_total_votes()} votes)'
    print(message)

@discord_client.tree.command(description="Requests a track to be possibly played")
@app_commands.check(is_taking_requests)
@app_commands.checks.cooldown(2, 120) # TODO: Remove cooldown, since we got a check if a user already submitted a request?
@app_commands.autocomplete(track=track_autocomplete)
async def request(interaction: discord.Interaction, track: str):
    '''Handle the request when user hits enter'''
    #Extract track incase the user did not select a track from autocompletion
    requested_track = await try_extracting_track(track)
    
    #Check if a track was extracted
    if not requested_track:
        #Check if submission is a selected track from the autocompletion
        if not track.startswith("spotify:track:"):
            await interaction.response.send_message("You don't have selected a track out of the list", ephemeral=True)
            return False

        requested_track = spotify_client.track(track, market='NL')

    #Limit track length then 10 mins
    if requested_track.get("duration_ms") >= (MAX_TRACK_LENGTH * 60 * 1000):
        await interaction.response.send_message("Requested track is too long", ephemeral=True)
        return False
        
    request_item = music_request.Request(interaction.user, requested_track)

    #Check if requested track is in playlist
    for played_track in spotify_client.playlist_tracks(SPOTIFY_PLAYLIST_ID).get("items"):
        if request_item.track.id in played_track.get("track").get("id"):
            await interaction.response.send_message("Requested track has already been played before", ephemeral=True)
            return False
        
    #Check if requested track is in queued collection of requests
    if collection.has_track_request(request_item):
        await interaction.response.send_message("Requested track is already being requested", ephemeral=True)
        return False
        
    #If requests is added to the list
    if collection.add_request(request_item):
        message = f"{request_item.user.display_name} has requested : {request_item.track}"
        print(message)
        await interaction.response.send_message(message)
        
        message = await interaction.original_response()
        request_item.message = message
    else:
        message = "Track was not submitted because you already submitted a request"
        await interaction.response.send_message(message, ephemeral=True)
        print(f"({interaction.user.display_name})" +message)
        
@request.error
async def request_error(interaction: discord.Interaction, error: app_commands.AppCommandError):
    if isinstance(error, app_commands.CheckFailure):
        if not collection.is_taking_requests():
            await interaction.response.send_message("Bot is currently not taking any requests", ephemeral=True)

    if isinstance(error, app_commands.CommandOnCooldown):
        await interaction.response.send_message(str(error), ephemeral=True)

@discord_client.tree.command(description="Shows the Spotify playlist of already played tracks")
async def playlist(interaction: discord.Interaction):
    '''Handle the request when user hits enter'''
    message = spotify_client.playlist(SPOTIFY_PLAYLIST_ID).get("external_urls").get("spotify")
    await interaction.response.send_message(message, ephemeral=True)

@discord_client.tree.command(description="Shows a list of the current submitted requests")
async def stats(interaction: discord.Interaction):
    '''Handle the request when user hits enter'''
    await interaction.response.send_message(content=collection.show_stats(), ephemeral=not is_admin(interaction))

#run discordbot
discord_client.run(token=DISCORD_TOKEN, reconnect=True)