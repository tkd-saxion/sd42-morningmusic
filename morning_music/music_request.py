import discord, spotipy, random

#Votes
UP_VOTE = '👍'
DOWN_VOTE = '👎'

class Track:
    def __init__(self, id: str, title: str, artists: list):
        self.id = id
        self.title = title
        self.artists = artists

    def str_artists(self):
        message = ""
        for artist in self.artists:
            message += artist['name'] + ' '
        return message

    def __str__(self):
        return f"{self.title} - {self.str_artists()}" 
        

class Request:
    def __init__(self, user: discord.User, track: dict):
        self.user : discord.User = user
        self.track = Track(track['id'], track['name'], track['artists'])
        self.message : discord.Message = None
        self.votes: dict[discord.User, int] = {}
        self.votes[user] = 1
    
    def __str__(self):
        return self.user.display_name + ": " + self.track.str_track_title()

    def get_total_votes(self) -> int:
        return sum([vote for vote in self.votes.values()])

class MorningRequests(list):
    '''Collection with extra functions'''

    def __init__(self):
        super().__init__()
        self._taking_requests = False

    def add_request(self, request: Request):
        if not self.has_user_request(request):
            self.append(request)
            return True
        
        return False

    def has_user_request(self, request: Request):
        for req in self:
            if req.user.id == request.user.id:
                return True
        
        return False

    def has_track_request(self, request: Request):
        for req in self:
            if req.track.id == request.track.id:
                return True
            
        return False

    def get_request(self, message_id: int) -> Request:
        for req in self:
            if req.message.id == message_id:
                return req
            
        return None
    
    def show_stats(self):
        if len(self) == 0:
            return "There are no requests made, Use /request to make one!"
        
        message = "#"*50+"\n"
        message += f"STATS (Requests: {len(self)})\n"
        message += "#"*50+"\n"
        for req in self:
            message += f"- {req.user.display_name}: {req.track} (Votes: {req.get_total_votes()})\n"
        return message

    def pick_a_request(self):
        if len(self) == 0:
            return False

        for _ in range(5):
            random.shuffle(self)

        weights = []
        for request in self:
            total_votes = 0
            for value in request.votes.values():
                total_votes += value

            #Catch votes lower then 1
            if total_votes < 1:
                total_votes = 1

            weights.append(total_votes)

        return random.choices(self, weights=weights, k=1).pop()

    def clear_requests(self):
        #Clear request/track objects
        for req in self:
            del req.track
            del req

        #Clear list itself
        self.clear()

    def set_taking_requests(self, state: bool):
        self._taking_requests = state

    def is_taking_requests(self) -> bool:
        return self._taking_requests

    async def change_vote(self, user: discord.User, target: Request, value: int = 0):
        #Get the request if the user already voted that request
        for req in self:
            #Remove vote
            if req.votes.get(user):
                del req.votes[user]

                #Check only previously voted messages and not current OR same message with a different reaction
                if req == target and not req.votes.get(user) == value:
                    if value == 1:
                        vote = DOWN_VOTE
                    elif value == -1:
                        vote = UP_VOTE
                        
                    if vote: 
                        await req.message.remove_reaction(vote, user)
                else: 
                    await req.message.remove_reaction(UP_VOTE, user)
                    await req.message.remove_reaction(DOWN_VOTE, user)

        #Check if 
        if value != 0:
            #Apply new vote
            target.votes[user] = value
    